#!/bin/bash

echo -e -n "\e[0m" ## Reset de colores en la terminal
echo -e "\n-------------------"
echo -e " \e[42m     \e[0m   \e[1mBuenos\e[0m"
echo -e " \e[42m     \e[0m   \e[1mAires\e[0m"
echo -e " \e[42m  \e[0m      Provincia"
echo -e "-------------------"
echo -e -n "\e[0m"

echo -e "Iniciando respaldo de base de datos de servidor de inventario..."

echo -e "Ingrese contraseña de base de datos: "
stty -echo
read db_password
stty echo
echo
echo -e "Contraseña leída."

db_file=snipeit-backup-$(date +%F_%R).sql

ssh_db_file="mysqldump -u root -p$db_password snipeit > /var/www/backups/db/$db_file"

echo -e "Exportando base de datos en servidor de producción"
ssh inventario.dpt.gba.gob.ar $ssh_db_file
echo -e "Archivo de base de datos generado con nombre: $db_file"

echo -e "Copiando base de datos en servidor de respaldo"
rsync -a root@inventario.dpt.gba.gob.ar:/var/www/backups/db/$db_file /var/lib/snipeit/db/$db_file
echo -e "Importando base de datos en servidor de respaldo..."
#Importar base de datos dentro de contenedor
echo -e "Ingrese ID de contenedor de base de datos"
read id_db
docker exec -i $id_db mysql -u root -p$db_password snipeit < /var/lib/snipeit/db/$db_file

echo -e "Sincronizando directorio public/uploads..."
rsync -a --progress root@inventario.dpt.gba.gob.ar:/var/www/html/snipeit/public/uploads/ /var/lib/snipeit/data/uploads/

echo -e "Sincronizando directorio storage/private_uploads..."
rsync -a --progress root@inventario.dpt.gba.gob.ar:/var/www/html/snipeit/storage/private_uploads/ /var/lib/snipeit/data/private_uploads/

#Realizar la migración interna del framework

echo -e "Ingrese ID de contenedor del servicio web de inventario"
read id_inventario

docker exec -it $id_inventario php artisan migrate

docker exec -it $id_inventario php artisan config:clear

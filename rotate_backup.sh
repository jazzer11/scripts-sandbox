#!/bin/bash

var=$(du -s /home/ftpuser/ftp/upload | cut -f1)

if [ $var -gt 35000000 ]
        then
        find /home/ftpuser/ftp/upload -maxdepth 1 -type f -mtime +3 -delete
fi

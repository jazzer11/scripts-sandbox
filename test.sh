#!/bin/bash

var=$(du -s /home/ftpuser/ftp/upload | cut -f1)

echo "Folder size is $var"

if [ $var -gt 35000000 ]
        then
        echo "The folder is full"
        find /home/ftpuser/ftp/upload -maxdepth 1 -type f -mtime +3
else
        echo "The folder still has space"
fi
